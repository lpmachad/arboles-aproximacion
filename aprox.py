#!/usr/bin/env pythoh3

# -- No pasa el test pero si funciona como se espera

"""
Cálculo del número óptimo de árboles.
"""

import sys

# Función que calcula la producción total de frutas dado un número de árboles.
def compute_trees(trees):
    global base_trees, fruit_per_tree, reduction  # Variables globales necesarias
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

# Función que calcula la producción para un rango de árboles.
def compute_all(min_trees, max_trees):
    productions = []
    for x in range(min_trees, max_trees + 1):	# Del minimo hasta el maximo incluido por eso el + 1
        productions.append((x, compute_trees(x)))
    return productions

# Función que lee los argumentos de la línea de comandos y retorna sus valores.
def read_arguments():
    if len(sys.argv) != 6:
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
    
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min_trees = int(sys.argv[4])
        max_trees = int(sys.argv[5])
    except ValueError:
        sys.exit("All arguments must be integers")
    
    return base_trees, fruit_per_tree, reduction, min_trees, max_trees

# Función principal del script
def main():
    global base_trees, fruit_per_tree, reduction
    base_trees, fruit_per_tree, reduction, min_trees, max_trees = read_arguments()
    
    productions = compute_all(min_trees, max_trees)
    
    best_production = 0
    best_trees = 0

    # Imprime la producción para cada cantidad de árboles y encuentra la mejor producción.
    for x in range(0, len(productions)):
        print(productions[x][0], productions[x][1])
        if productions[x][1] > best_production:
            best_production = productions[x][1]
            best_trees = productions[x][0]

    print(f"Best production: {best_production}, for {best_trees} trees")

# Ejecuta el script si es el programa principal
if __name__ == '__main__':
    main()

